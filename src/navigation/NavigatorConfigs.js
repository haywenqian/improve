import React from "react";
import Scene from "../scenes";
import {createStackNavigator} from "react-navigation";

const HomeNavigator = createStackNavigator({
    Home:{
        screen: Scene.Home,
        navigationOptions: ({navigation}) => ({
            header: null
        })
    }
});

const ProfileNavigator = createStackNavigator ({
   Profile: {
       screen: Scene.Profile,
       navigationOptions: ({navigation}) => ({
           header: null
       })
   }
});

export {
    HomeNavigator,
    ProfileNavigator
}
