import {NavigationActions} from "react-navigation";

let _navigator;

function setTopLevelNavigator (navigatorRef) {
    _navigator = navigatorRef;
}

function navigate (routeName, params, action) {
    _navigator.dispatch(
        NavigationActions.navigate({
            type: NavigationActions.NAVIGATE,
            routeName,
            params,
            action
        })
    );
}

function dispatch (action){
    _navigator.dispatch(action)
}

export default {navigate, setTopLevelNavigator, dispatch};
