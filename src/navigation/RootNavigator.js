import React from "react";
import {} from "react-native";
import NavigationService from "./NavigationService";
import AppNavigator from "./AppRouteConfigs";

class RootNavigator extends React.Component {
    render(){
        return(
            <AppNavigator
                ref = {navigatorRef => {
                    NavigationService.setTopLevelNavigator(navigatorRef)
                }}
            />
        )
    }
}

export default RootNavigator;
