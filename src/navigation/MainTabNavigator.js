import React from "react";
import {createBottomTabNavigator} from "react-navigation";
import {HomeNavigator, ProfileNavigator} from "./NavigatorConfigs";

export default createBottomTabNavigator ({
   Home: HomeNavigator,
   Profile: ProfileNavigator
},{
    initialRouteName: "Home",
    navigationOptions: ({navigation}) => ({
        tabBarOnPress: ({navigation}) => {
            const {state} = navigation;

            if(navigation.isFocused()){
                if(state.routes && state.routes.length > 1){

                }
            }else {
                const {key} = state;
                return navigation.navigate(key);
            }
        }
    }),
    tabBarPosition: "bottom",
    tabBarOptions: {
        showLabel: true,
        allowFontScaling: false,
        activeTintColor: "red",
        inactiveTintColor: "black"
    },
    animationEnabled: false,
    swipeEnabled: false,
    lazy: true,
    removeClippedSubviews: true
});
