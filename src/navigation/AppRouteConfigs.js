import React from "react";
import {createAppContainer, createStackNavigator, createSwitchNavigator} from "react-navigation";
import MainTabNavigator from "./MainTabNavigator";
import Scenes from "../scenes";

const AppNavigator = createStackNavigator({
    MainScreen: MainTabNavigator
},{
    headerMode: "none"
});

const MainNavigator = createSwitchNavigator({
   App: AppNavigator
});

export default createAppContainer(MainNavigator);
