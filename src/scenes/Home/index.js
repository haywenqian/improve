import React, { PureComponent } from "react";
import { View, Text, RefreshControl, StyleSheet } from "react-native";
import HomeList from "./components/HomeList";
import Helper from "../../utils/Helper";

const data = {
  items: [
    {
      id: 1,
      name: "Message 1",
      venue:
        "RT4-3A-01, Level 4, Stellar Walk, Jalan Persiaran Senibong, Bandar Baru Permas Jaya, 81750 Masai, Johor Bahru, Johor."
    },
    {
      id: 2,
      name: "Message 2",
      venue:
        "RT4-3A-01, Level 4, Stellar Walk, Jalan Persiaran Senibong, Bandar Baru Permas Jaya, 81750 Masai, Johor."
    }
  ]
};

class Home extends PureComponent {
  onEndReached = () => {};

  render() {
    return (
      <View style={styles.container}>
        <HomeList data={data.items} flatListStyle={styles.flatListStyle} />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "gray",
    paddingHorizontal: 10,
    paddingTop: Helper.IS_IOS ? 20 : 0
  },
  flatListStyle: {
    paddingVertical: 15
  }
});

export default Home;
