import React, { PureComponent } from "react";
import { View, StyleSheet } from "react-native";
import CustomFlatList from "../../../components/CustomFlatList";
import HomeListItems from "./components/HomeListItems";

class HomeList extends PureComponent {
  renderItem = item => {
    return <HomeListItems data={item.item} />;
  };

  renderSeparator = () => {
    return <View style={{ marginVertical: 10 }} />;
  };

  render() {
    const { data, flatListStyle } = this.props;
    return (
      <CustomFlatList
        data={data}
        renderItem={this.renderItem}
        contentContainerStyle={flatListStyle}
        ItemSeparatorComponent={this.renderSeparator}
      />
    );
  }
}

export default HomeList;
