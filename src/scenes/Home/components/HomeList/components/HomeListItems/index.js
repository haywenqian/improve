import React, { PureComponent } from "react";
import { View, StyleSheet } from "react-native";
import CustomText from "../../../../../components/CustomText";

class HomeListItems extends PureComponent {
  render() {
    const { data } = this.props;

    return (
      <View style={styles.container}>
        <CustomText>{data.name}</CustomText>
        <CustomText>{data.venue}</CustomText>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "white",
    borderRadius: 8,
    paddingHorizontal: 10,
    paddingVertical: 10
  }
});

export default HomeListItems;
