import React from "react";
import { FlatList } from "react-native";

const CustomFlatList = props => {
  function _keyExtractor(item, index) {
    return index.toString();
  }

  return (
    <FlatList
      initialNumToRender={10}
      keyExtractor={_keyExtractor}
      maxToRenderPerBatch={10}
      onEndReachedThreshold={0.2}
      showsVerticalScrollIndicator={false}
      showsHorizontalScrollIndicator={false}
      {...props}
    />
  );
};

export default CustomFlatList;
