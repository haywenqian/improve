import * as React from "react";
import { Text, StyleSheet } from "react-native";

const CustomText = props => {
  const { style, size, children } = props;
  return (
    <Text
      {...props}
      style={[style, { fontSize: size }]}
      allowFontScaling={false}
    >
      {children}
    </Text>
  );
};

const styles = StyleSheet.create({
  text: {}
});

export default CustomText;
